# Project Title
KATA Ecommerce App

## Demo link:
Access my site at [google.com](https://google.com)

## Table of Content:

- [About The App](#about-the-app)
- [Technologies](#technologies)
- [Setup](#setup)
- [Status](#status)

## About The App
[KATA Ecommerce App] is an ecommerce app for clothes, jewelery and electronics.

## Technologies
I used `React`, `Bootstrap`.

## Setup
- download or clone the repository
- run `npm install`
- run `npm start`


## Status
[KATA Ecommerce App] is still in progress.
