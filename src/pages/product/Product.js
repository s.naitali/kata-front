import React, {useContext} from 'react';
import {ProductContext} from "../../context/ProductContext";
import {useParams} from "react-router-dom";
import SingleProduct from "../../components/singleProduct/SingleProduct";

const Product = () => {
    const {products} = useContext(ProductContext);
    const {productId} = useParams();
    const product = products.find((e) => e.id === Number(productId));
    return (
        <div>
            <SingleProduct product={product}/>
        </div>
    );
};

export default Product;