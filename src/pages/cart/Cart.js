import React, {useContext} from 'react';
import './Cart.css';
import {
    FaCcMastercard,
    FaCcPaypal,
    FaCcVisa,
    FaLongArrowAltLeft,
    FaLongArrowAltRight,
    FaRegCreditCard,
    FaTrashAlt
} from "react-icons/fa";
import {ProductContext} from "../../context/ProductContext";

const Cart = () => {
    const {products, cartItems, deleteItemFromCart, getTotalCartItemPriceAmount, getTotalItemsInCart} = useContext(ProductContext);
    return (
        <section className="h-100" style={{backgroundColor: "#eee"}}>
            <div className="container py-5 h-100">
                <div className="row d-flex justify-content-center align-items-center h-100">
                    <div className="col">
                        <div className="card">
                            <div className="card-body p-4">

                                <div className="row">

                                    <div className="col-lg-7">
                                        <h5 className="mb-3"><a href="#" className="text-body"><FaLongArrowAltLeft className="me-2"/>Continue shopping</a></h5>
                                        <hr/>

                                        <div className="d-flex justify-content-between align-items-center mb-4">
                                            <div>
                                                <p className="mb-1">Shopping cart</p>
                                                <p className="mb-0">You have {getTotalItemsInCart()} items in your cart</p>
                                            </div>
                                        </div>

                                        {products.map((product) => {
                                            if(cartItems[product.id] > 0) {
                                                return (
                                                    <div className="card mb-3">
                                                        <div className="card-body">
                                                            <div className="d-flex justify-content-between">
                                                                <div className="d-flex flex-row align-items-center">
                                                                    <div>
                                                                        <img
                                                                            src={product.image}
                                                                            className="img-fluid rounded-3"
                                                                            alt="Shopping item"
                                                                            style={{width: "65px"}}/>
                                                                    </div>
                                                                    <div className="ms-3">
                                                                        <p>{product.title}</p>
                                                                    </div>
                                                                </div>
                                                                <div className="d-flex flex-row align-items-center">
                                                                    <div style={{width: "50px"}}>
                                                                        <p className="fw-normal mb-0">{cartItems[product.id]}</p>
                                                                    </div>
                                                                    <div style={{width: "80px"}}>
                                                                        <p className="mb-0">{Math.floor(product.price*cartItems[product.id]*100)/100}  &euro;</p>
                                                                    </div>
                                                                    <div style={{color: "darkslategrey", cursor: "pointer"}}
                                                                       onClick={() => {
                                                                           deleteItemFromCart(product.id)
                                                                       }}><FaTrashAlt/></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                )
                                            }
                                            return null;
                                        })}

                                    </div>
                                    <div className="col-lg-5">

                                        <div className="card bg-primary text-white rounded-3">
                                            <div className="card-body">
                                                <div className="d-flex justify-content-between align-items-center mb-4">
                                                    <h5 className="mb-0">Card details</h5>
                                                    <h1>KATA</h1>
                                                </div>

                                                <p className="small mb-2">Card type</p>
                                                <a href="#!" type="submit" className="text-white"><FaCcVisa
                                                    className="me-2 payment-icons"/></a>
                                                <a href="#!" type="submit" className="text-white"><FaCcMastercard
                                                    className="me-2 payment-icons"/></a>
                                                <a href="#!" type="submit" className="text-white"><FaRegCreditCard
                                                    className="me-2 payment-icons"/></a>
                                                <a href="#!" type="submit" className="text-white"><FaCcPaypal
                                                    className="me-2 payment-icons"/></a>

                                                <form className="mt-4">
                                                    <div className="form-outline form-white mb-4">
                                                        <input type="text" id="typeName"
                                                               className="form-control form-control-lg" siez="17"
                                                               placeholder="Cardholder's Name"/>
                                                        <label className="form-label" htmlFor="typeName">Cardholder's
                                                            Name</label>
                                                    </div>

                                                    <div className="form-outline form-white mb-4">
                                                        <input type="text" id="typeText"
                                                               className="form-control form-control-lg" siez="17"
                                                               placeholder="1234 5678 9012 3457" minLength="19"
                                                               maxLength="19"/>
                                                        <label className="form-label" htmlFor="typeText">Card
                                                            Number</label>
                                                    </div>

                                                    <div className="row mb-4">
                                                        <div className="col-md-6">
                                                            <div className="form-outline form-white">
                                                                <input type="text" id="typeExp"
                                                                       className="form-control form-control-lg"
                                                                       placeholder="MM/YYYY" size="7" id="exp"
                                                                       minLength="7" maxLength="7"/>
                                                                <label className="form-label"
                                                                       htmlFor="typeExp">Expiration</label>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <div className="form-outline form-white">
                                                                <input type="password" id="typeText"
                                                                       className="form-control form-control-lg"
                                                                       placeholder="&#9679;&#9679;&#9679;" size="1"
                                                                       minLength="3" maxLength="3"/>
                                                                <label className="form-label"
                                                                       htmlFor="typeText">Cvv</label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </form>

                                                <hr className="my-4"/>

                                                <div className="d-flex justify-content-between">
                                                    <p className="mb-2">Subtotal</p>
                                                    <p className="mb-2">{getTotalCartItemPriceAmount()}  &euro;</p>
                                                </div>

                                                <div className="d-flex justify-content-between">
                                                    <p className="mb-2">Shipping</p>
                                                    <p className="mb-2">Free</p>
                                                </div>

                                                <div className="d-flex justify-content-between mb-4">
                                                    <p className="mb-2">Total(Incl. taxes)</p>
                                                    <p className="mb-2">{getTotalCartItemPriceAmount()}  &euro;</p>
                                                </div>

                                                <button type="button" className="btn btn-info btn-block btn-lg w-100">
                                                    <div className="d-flex justify-content-between">
                                                        <span>{getTotalCartItemPriceAmount()}  &euro;</span>
                                                        <span>Checkout <FaLongArrowAltRight/></span>
                                                    </div>
                                                </button>

                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Cart;