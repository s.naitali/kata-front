import React, {useContext} from 'react';
import {ProductContext} from "../../context/ProductContext";
import Item from "../../components/item/Item";
import './ShopCategory.css';

const ShopCategory = (props) => {
    const {products, addItemToCart} = useContext(ProductContext);
    return (
        <div>
            <div className="container py-5">
                <div className="row d-flex justify-content-center mb-5">
                    <div className="col-8 d-flex justify-content-center header-cat">
                        <h1 className="display-3 fw-semibold">
                            {props.category}
                        </h1>
                    </div>
                </div>
                <div className="row d-flex flex-wrap">
                    {
                        products.map(item => {
                            if (props.category === item.category) {
                                return (
                                    <Item
                                        key={item.id}
                                        ids={item.id}
                                        img={item.image}
                                        title={item.title}
                                        description={item.description}
                                        price={item.price}
                                        addToCart={() => addItemToCart(item.id)}
                                    />
                                )
                            } else {
                                return null;
                            }
                        })
                    }
                </div>
            </div>
        </div>
    );
};

export default ShopCategory;