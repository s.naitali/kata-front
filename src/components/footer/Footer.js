import React from 'react';
import './Footer.css';
import {
    FaCcMastercard,
    FaCcPaypal, FaCcVisa,
    FaFacebook,
    FaInstagram,
    FaLongArrowAltRight,
    FaPinterest,
    FaRegCreditCard,
    FaTwitter
} from "react-icons/fa";
const Footer = () => {
    return (
        <div>
            <footer>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-3 col-sm-6">
                            <div className="single-box">
                                <h1>KATA</h1>
                                <p>No, 144 Zerktouni, Casablanca <br/> Morocco</p>
                                <p>+212-522476512</p>
                                <p>support-kata@kata.com</p>
                                <h3>We accept</h3>
                                <div className="card-area">
                                    <FaCcVisa className="icon-footer"/>
                                    <FaCcMastercard className="icon-footer"/>
                                    <FaRegCreditCard className="icon-footer"/>
                                    <FaCcPaypal className="icon-footer"/>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-sm-6">
                            <div className="single-box">
                                <h2>Information</h2>
                                <ul>
                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">Refund Policy</a></li>
                                    <li><a href="">Shipping Policy</a></li>
                                    <li><a href="">Terms of Service</a></li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-lg-3 col-sm-6">
                            <div className="single-box">
                                <h2>Account</h2>
                                <ul>
                                    <li><a href="">About us</a></li>
                                    <li><a href="">Contact</a></li>
                                    <li><a href="">FAQ</a></li>
                                    <li><a href="">Careers</a></li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-lg-3 col-sm-6">
                            <div className="single-box">
                                <h2>NewsLetter</h2>
                                <p>Wearing will never be the same</p>
                                <div className="input-group mb-3">
                                    <input type="text" className="form-control" placeholder="Enter Your Email"
                                           aria-label="Recipient's username" aria-describedby="basic-addon2"/>
                                    <span className="input-group-text" id="basic-addon2"><FaLongArrowAltRight /></span>
                                </div>
                                <h2>Follow Us On</h2>
                                <p>
                                    <FaFacebook className="socials"/>
                                    <FaInstagram className="socials"/>
                                    <FaTwitter className="socials"/>
                                    <FaPinterest className="socials"/>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <footer className="p-2">
                <div className="container-xxl">
                    <div className="row">
                        <div className="col-12">
                            <p className="text-center mb-0 text-white">
                                &copy; {new Date().getFullYear()}, Powered by KATA
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    );
};

export default Footer;