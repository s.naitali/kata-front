import React from 'react';
import './Item.css';
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap"
import {Link} from "react-router-dom";

const Item = (props) => {
    return (
        <div className="col-lg-4 col-md-6 col-sm-6 mb-5">
            <div className="card border-0 rounded-0 shadow h-100">
                <img src={props.img} className="card-img-top rounded-0" alt="..." height="250px"/>
                <div className="card-body d-flex flex-column flex-grow-1 my-3">
                    <div className="row flex-grow-1">
                            <h4 className="card-title">{props.title}</h4>
                    </div>
                    <div className="row">
                        <div className="col-7 item-price">
                            {props.price}  &euro;
                        </div>
                    </div>
                </div>
                <div className="row align-items-center text-center g-0">
                    <div className="col-md-6 col-sm-12">
                        <Link className="btn btn-secondary text-white p-2 w-100 rounded-0" to={`/product/${props.ids}`} onClick={()=> console.log(props)}>DETAILS</Link>
                    </div>
                    <div className="col-md-6 col-sm-12">
                        <button className="btn btn-dark text-white p-2 w-100 rounded-0" onClick={props.addToCart}>ADD TO CART</button>
                    </div>
                </div>
            </div>
        </div>
)
    ;
};

export default Item;