import React from 'react';
import './Hero.css';
import hero_img from "../assets/hero-image-shop.jpg";

const Hero = () => {
    return (
        <div>
            <div id="carouselExampleDark" className="carousel carousel-dark slide">
                <div className="carousel-inner">
                    <div className="carousel-item active" data-bs-interval="10000">
                        <img src={hero_img} className="d-block w-100" alt="..."/>
                        <div className="carousel-caption d-none d-md-block">
                            <h5>Summer, New Collections</h5>
                            <p>Lets discover the desire of wearing together</p>
                            <p>Please, Choose the section you want to explore</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Hero;