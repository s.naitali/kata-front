import React, {useContext, useEffect, useState} from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap"
import {IoCartOutline} from "react-icons/io5";
import {Link} from "react-router-dom";
import {BsSearch} from "react-icons/bs";
import "./Navbar.css"
import {ProductContext} from "../../context/ProductContext";

const Navbar = () => {
    const [activeLink, setActiveLink] = useState("");
    const {getTotalItemsInCart} = useContext(ProductContext);
    useEffect(() => {
        const path = window.location.pathname;
        setActiveLink(path.slice(1, path.length));
    }, []);
    return (
        <div>
            <nav className="navbar navbar-dark navbar-expand-lg bg-dark">
                <div className="container">
                    <Link onClick={() => setActiveLink("")} className="navbar-brand me-auto" to={"/"}>KATA</Link>
                    <div className="offcanvas offcanvas-end" tabIndex="-1" id="offcanvasNavbar"
                         aria-labelledby="offcanvasNavbarLabel">
                        <div className="offcanvas-header">
                            <h5 className="offcanvas-title" id="offcanvasNavbarLabel">KATA</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="offcanvas"
                                    aria-label="Close"></button>
                        </div>
                        <div className="offcanvas-body align-items-center">
                            <ul className="navbar-nav justify-content-center flex-grow-1 pe-3">
                                <li className="nav-item">
                                    <Link
                                        onClick={() => setActiveLink("")}
                                        className={
                                            activeLink === '' ?
                                                'nav-link mx-lg-2 active' :
                                                'nav-link mx-lg-2'
                                        }
                                        to={"/"}
                                    >
                                        Shop
                                    </Link>
                                </li>
                                <li className="nav-item">
                                    <Link
                                        onClick={() => setActiveLink("women")}
                                        className={
                                            activeLink === 'women' ?
                                                'nav-link mx-lg-2 active' :
                                                'nav-link mx-lg-2'
                                        }
                                        to={"/women"}
                                    >
                                        Women
                                    </Link>
                                </li>
                                <li className="nav-item">
                                    <Link
                                        onClick={() => setActiveLink("men")}
                                        className={
                                            activeLink === 'men' ?
                                                'nav-link mx-lg-2 active' :
                                                'nav-link mx-lg-2'
                                        }
                                        to={"/men"}
                                    >
                                        Men
                                    </Link>
                                </li>
                                <li className="nav-item">
                                    <Link
                                        onClick={() => setActiveLink("jewelery")}
                                        className={
                                            activeLink === 'jewelery' ?
                                                'nav-link mx-lg-2 active' :
                                                'nav-link mx-lg-2'
                                        }
                                        to={"/jewelery"}
                                    >
                                        Jewelery
                                    </Link>
                                </li>
                                <li className="nav-item">
                                    <Link
                                        onClick={() => setActiveLink("electronics")}
                                        className={
                                            activeLink === 'electronics' ?
                                                'nav-link mx-lg-2 active' :
                                                'nav-link mx-lg-2'
                                        }
                                        to={"/electronics"}
                                    >
                                        Electronics
                                    </Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="me-4">
                        <Link className="position-relative" to={"/cart"}>
                            <IoCartOutline className="text-white header-icons"/>
                            <span
                                className="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-light text-black">{getTotalItemsInCart()}<span
                                className="visually-hidden">items</span></span>
                        </Link>
                    </div>
                    <button className="navbar-toggler" type="button" data-bs-toggle="offcanvas"
                            data-bs-target="#offcanvasNavbar" aria-controls="offcanvasNavbar">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                </div>
            </nav>
        </div>
    );
};

export default Navbar;