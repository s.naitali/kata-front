import React, {useContext} from 'react';
import './SingleProduct.css';
import {ProductContext} from "../../context/ProductContext";

const SingleProduct = (props) => {
    const {product} = props;
    const {addItemToCart} = useContext(ProductContext);
    return (
        <div>
            <div className="container my-5">
                <div className="row">
                    <div className="col-md-5">
                        <div className="main-img">
                            <img className="img-fluid" src={product.image} alt="ProductS"/>
                        </div>
                    </div>
                    <div className="col-md-7">
                        <div className="main-description px-2">
                            <div className="category text-bold">
                                Category: {product.category}
                            </div>
                            <div className="product-title text-bold my-3">
                                {product.title}
                            </div>


                            <div className="price-area my-4">
                                <p className="new-price text-bold mb-1">{product.price}  &euro;</p>
                                <p className="text-secondary mb-1">(Additional tax may apply on checkout)</p>

                            </div>
                            <div className="d-flex flex-row my-3">
                                <div className="text-warning mb-1 me-2">
                                    <span className="ms-1 text-bold">
                                        {product.rating.rate}
                                    </span>
                                </div>
                                <span className="text-muted">154 orders</span>
                                <span className="text-success ms-2">In stock</span>
                            </div>


                            <div className="buttons d-flex my-5">
                                <div className="block">
                                    <a href="#" className="shadow btn custom-btn ">Wishlist</a>
                                </div>
                                <div className="block">
                                    <button className="shadow btn custom-btn" onClick={()=>{addItemToCart(product.id)}}>Add to cart</button>
                                </div>
                            </div>

                        </div>

                        <div className="product-details my-4">
                            <p className="details-title text-color mb-1">Product Details</p>
                            <p className="description">{product.description}</p>
                        </div>

                        <div className="row questions bg-light p-3">
                            <div className="col-md-1 icon">
                                <i className="fa-brands fa-rocketchat questions-icon"></i>
                            </div>
                            <div className="col-md-11 text">
                                Have a question about our products? Feel free to contact our representatives via live
                                chat or email.
                            </div>
                        </div>

                        <div className="delivery my-4">
                            <p className="font-weight-bold mb-0"><span><i className="fa-solid fa-truck"></i></span> <b>Delivery done in 3 days from date of purchase</b> </p>
                            <p className="text-secondary">Order now to get this product delivery</p>
                        </div>
                        <div className="delivery-options my-4">
                            <p className="font-weight-bold mb-0"><span><i className="fa-solid fa-filter"></i></span> <b>Delivery options</b> </p>
                            <p className="text-secondary">View delivery options here</p>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    );
};

export default SingleProduct;