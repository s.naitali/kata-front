import React, {createContext, useEffect, useState} from "react";

export const ProductContext = createContext(null);
const getDefaultCart = () => {
    let cart = {};
    for (let i = 0; i < 122+1; i++) {
        cart[i] = 0;
    }
    return cart;
}

const ProductContextProvider = (props) => {
    const [products, setProducts] = useState([]);

    const  [cartItems, setCartItems] = useState(getDefaultCart());

    useEffect(() => {
        fetch("https://fakestoreapi.com/products")
            .then((resp) => resp.json())
            .then((data) => setProducts(data))
    }, []);

    const addItemToCart = (itemId) => {
        setCartItems((previous) => ({
            ...previous,
            [itemId]: previous[itemId] + 1
        }));
    }

    const deleteItemFromCart = (itemId) => {
        setCartItems((previous) => ({
            ...previous,
            [itemId]: previous[itemId] - 1
        }))
    }

    const getTotalItemsInCart = () => {
        let total = 0;
        for(const item in cartItems) {
            if (cartItems[item] > 0) {
                total += cartItems[item];
            }
        }
        return total;
    }

    const getTotalCartItemPriceAmount = () => {
        let total = 0;
        for(const item in cartItems) {
            if (cartItems[item] > 0) {
                 let itemInfo = products.find((prdct) => prdct.id === Number(item));
                 total += itemInfo.price * cartItems[item];
            }
        }
        return Math.floor(total*100)/100;
    }

    const contextValue = {products, cartItems, addItemToCart, deleteItemFromCart, getTotalCartItemPriceAmount, getTotalItemsInCart};

    return (
        <ProductContext.Provider value={contextValue}>
            {props.children}
        </ProductContext.Provider>
    )
}

export default ProductContextProvider;