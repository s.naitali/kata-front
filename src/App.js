import React from 'react';
import './App.css';
import {BrowserRouter, Routes, Route} from "react-router-dom";
import Layout from "./pages/layout/Layout";
import Shop from "./pages/shop/Shop";
import ShopCategory from "./pages/shopCategory/ShopCategory";
import Product from "./pages/product/Product";
import Cart from "./pages/cart/Cart";

function App() {
  return (
    <div>
      <BrowserRouter>
          <Routes>
              <Route path={"/"} element={<Layout/>}>
                  <Route index element={<Shop/>}/>
                  <Route index path={"/women"} element={<ShopCategory category={"women's clothing"}/>}/>
                  <Route path={"/men"} element={<ShopCategory category={"men's clothing"}/>}/>
                  <Route path={"/jewelery"} element={<ShopCategory category={"jewelery"}/>}/>
                  <Route path={"/electronics"} element={<ShopCategory category={"electronics"}/>}/>
                  <Route path={"/product"} element={<Product/>}>
                      <Route path={":productId"} element={<Product/>}/>
                  </Route>
                  <Route path={"/cart"} element={<Cart/>}/>
                  <Route index element={<Shop/>}/>
              </Route>
          </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
